/**
 * Created by zendynamix on 11-Sep-16.
 */
taxiFleetManager.controller("taxiTrailController", function ($scope, taxiTrailService, leafLetMapService) {
    $scope.taxiTrailController = {
        showHistoryLoading: false,
        toTime: 24,
        fromTime: 1,
        filter: {
            carList: [],
            carSelected: {name: '', id: 0},
            fromDate: new Date("2017-10-11"),
            toDate: new Date(),
            fromTime: new Date().getHours(),
            toTime: new Date().getHours()
        }
    }


    $scope.setTaxiSelected = function (taxiObj) {
        $scope.taxiTrailController.filter.carSelected.name = taxiObj.carName;
        $scope.taxiTrailController.filter.carSelected.id = taxiObj.carId;
    }
    function getObdDeviceLinkedCarList() {
        taxiTrailService.getAllTaxiLinkedToObdDevice()
            .then(function (data) {

                if (data.data[0] && data.data.length > 0) {
                    console.log("************data*************")
                    console.log(data.data[0].carId)
                    $scope.taxiTrailController.filter.carSelected.name = data.data[0].carName;
                    $scope.taxiTrailController.filter.carSelected.id = data.data[0].carId;
                    $scope.taxiTrailController.filter.carList = data.data;
                }

            })
    }

    /*  $scope.uiFromTime=null;
     $scope.uiToTime=null;*/
    $scope.showLoader=false;
    $scope.startTrailing = function () {
        $scope.showLoader=true
        leafLetMapService.deletePolyLine()
        var polgonArray = []
        $scope.showPlotsMessage = ""
        var fromDate = new Date($scope.taxiTrailController.filter.fromDate);
        var toDate = new Date($scope.taxiTrailController.filter.toDate);
        var fromTime = parseInt($scope.taxiTrailController.filter.fromTime);
        var toTime = parseInt($scope.taxiTrailController.filter.toTime);
        var carId = $scope.taxiTrailController.filter.carSelected.name;

        taxiTrailService.getHistoryPlotsBasedOnRange(fromDate, toDate, carId, fromTime, toTime)
            .then(function (data) {
                $scope.showLoader=false
                if (data.data.length > 0) {
                    for (var g = 0; g < data.data.length; g++) {
                        var obj = {}
                        obj.lat = data.data[g].lat
                        obj.lng = data.data[g].lng
                        polgonArray.push(obj)
                    }
                    leafLetMapService.drawPolylineMapBasedOnInterval(polgonArray)
                } else {
                    $scope.showPlotsMessage = "sorry, no data available for selected interval"
                }


            })
    }
    function init() {
        getObdDeviceLinkedCarList()
    }

    init();
})

