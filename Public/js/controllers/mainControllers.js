taxiFleetManager.controller("mainController", function ($scope, $location, $interval, $rootScope, carTrackPollerService) {

    $scope.currentRout = function (path) {
        var loc = $location.path();
        return loc.includes(path)
    };

    $scope.xAxisDay = ['6-10', '11-20', '21-30', '31-40', '41-50', '51-60', '61-70', '71-100'];
    $scope.yAxisDay = [{data: [40, 30, 80, 90, 60, 40, 30, 10]}];

    $scope.currentDate = null;
    $interval(function () {
        $scope.currentDate = new Date();
    }, 1000)


    // Create an array of styles.
    $scope.mapStyles = [

        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#838f9d"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "weight": 0.6
                },
                {
                    "color": "#1a3541"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1d212f"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1e2230"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1e2230"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#353b50"
                },
                {
                    "lightness": 10
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1a1e2a"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#616c8d"
                }
            ]
        }
    ];
    $scope.showNotification = false;
    $scope.notificationHideShow = function () {
        $scope.showNotification = !$scope.showNotification;
        $rootScope.$emit('mapResize')
    }
    $scope.stopPolling = function () {

        carTrackPollerService.stopDevice().then(function () {
            console.log("stoping")

        })

    }



});



