/**
 * Created by MohammedSaleem on 13/08/16.
 */

taxiFleetManager.controller("maintenanceBayController", function ($scope, $timeout, $rootScope,
                                                                  assetApiService, leafLetMapService,
                                                                  maintainceBayService) {


    function getAllDeviceFromApi() {
        assetApiService.getMapPlotsFromApi().then(function (result) {
            $scope.setTerminalView('terminal1');
            if (result) {
                /*  plotMarkersFromApi(result)*/
            }
        }, function error(errResponse) {
            console.log(errResponse);
        });
    }


    $scope.setTerminalView = function (terminal) {
        maintainceBayService.getMapCenterPlots().then(function (result) {
            leafLetMapService.setCenterLocationOfMap(
                result.data[0].mapDetailsSchemaModelConfiguration.detailsTab[terminal].mapCenterCoOrdinates.lat,
                result.data[0].mapDetailsSchemaModelConfiguration.detailsTab[terminal].mapCenterCoOrdinates.lng,
                result.data[0].mapDetailsSchemaModelConfiguration.detailsTab[terminal].mapCenterCoOrdinates.zoomLevel);

        })
    }


    function initFunction() {
        getAllDeviceFromApi()
    }

    initFunction();


});
