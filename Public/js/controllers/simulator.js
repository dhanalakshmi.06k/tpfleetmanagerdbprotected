/**
 * Created by MohammedSaleem on 10/03/16.
 */

taxiFleetManager.controller("simulatorCtrl", function ($scope, settingsService) {
    $scope.mapSettingsDetails = {
        taxiEntryExitStatus: settingsService.getTaxiEntryExitSimulatorFlag()

    }
    $scope.settIngSCtatus = {
        taxiEntryExitStatus: settingsService.getTaxiEntryExitSimulatorFlag(),

    };

    $scope.setSimulatorForTaxiEntryExit = function (flag) {
        if (!flag) {
            settingsService.stopBusScheduleSimulator();
            settingsService.setTaxiEntryExitSimulatorFlag(false)
            $scope.mapSettingsDetails.taxiEntryExitStatus = settingsService.getTaxiEntryExitSimulatorFlag();
        } else {
            settingsService.startBusScheduleSimulator();
            settingsService.setTaxiEntryExitSimulatorFlag(true)
            $scope.mapSettingsDetails.taxiEntryExitStatus = settingsService.getTaxiEntryExitSimulatorFlag();
        }

    }


});