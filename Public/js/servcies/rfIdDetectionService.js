/**
 * Created by Suhas on 9/9/2016.
 */

taxiFleetManager.service('rfIdDetectionService', function ($http) {
    var getHistoricalData = function (seriesSelected) {
        return $http.get('/smrt/rfIdDetection/historicalData/' + seriesSelected)
    }
    return {
        getHistoricalData: getHistoricalData
    }
})