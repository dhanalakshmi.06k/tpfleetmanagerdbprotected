/**
 * Created by Suhas on 7/28/2016.
 */
taxiFleetManager.factory("constantService", function ($http) {
    var historyObjDetails;
    var getHistroyConstants = function () {
        return $http.get('/settings/historyConfig')
    }

    var setDeviceHistroyConstant = function (historyObj) {
        historyObjDetails = historyObj;

    }
    var getDeviceHistroyConstant = function () {
        return historyObjDetails;
    }
    return {
        getHistroyConstants: getHistroyConstants,
        setDeviceHistroyConstant: setDeviceHistroyConstant,
        getDeviceHistroyConstant: getDeviceHistroyConstant

    }
})