/**
 * Created by zendynamix on 9/10/2016.
 */

taxiFleetManager.factory('carTrackPollerService', function ($http) {
    var startDevice = function (deviceName) {
        return $http.get('/dataTracker/trackCars/' + "start" + '/' + deviceName);
    }
    var stopDevice = function () {
        return $http.get('/dataTracker/trackCars/' + "stop"+'/'+"q");
    }
    return {
        startDevice: startDevice,
        stopDevice: stopDevice
    };
})