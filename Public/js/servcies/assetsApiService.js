/**
 * Created by zendynamix on 8/11/2016.
 */


taxiFleetManager.factory('assetApiService', function ($http, assetApiUrlService, sensorService, constantService) {
    var deviceId, deviceDirectionArray = [];
    var getDevicesDetails = function () {
        return assetApiUrlService.getDevices().then(function (result) {

            var deviceDetailsArray = [];
            for (var l = 0; l < result.data[0].items.length; l++) {

                var detailsObj = {};
                detailsObj.id = result.data[0].items[l].id
                detailsObj.name = result.data[0].items[l].name
                detailsObj.online = result.data[0].items[l].online
                detailsObj.lat = result.data[0].items[l].lat
                detailsObj.lng = result.data[0].items[l].lng
                detailsObj.createdDate = result.data[0].items[l].device_data.created_at
                detailsObj.updatedDate = result.data[0].items[l].device_data.updated_at
                detailsObj.tail = result.data[0].items[l].tail
                deviceDetailsArray.push(detailsObj);
                if (result.data[0].items.length - 1 == l) {
                    return deviceDetailsArray;
                }
            }
        }, function error(errResponse) {
            return errResponse;
        });
    }
    var getDeviceHistory = function (deviceId) {
        var historyConfigObject = constantService.getDeviceHistroyConstant()
        var fromDate = moment().utc().format('YYYY-MM-DD')
        var toDate = moment().utc().format('YYYY-MM-DD')
        //var fromTime=moment().utc().format('HH:mm');
        //var toTime=moment().utc().subtract(historyConfigObject.histroyConstants.TIME_INTERVAL,historyConfigObject.histroyConstants.TIME_INTERVAL_UINT).format('HH:mm');
        var fromTime = moment().utc() //.format('HH:mm');
        var toTime = moment().subtract(300, 'seconds').utc()//.format('HH:mm');
        /* var timeItem=[21,23,23,14,21,12,14,16,17,10]
         var fromDate='2016-8-15'
         var toDate='2016-8-15'
         var fromTime='00:00';
         var toTime=moment().subtract(timeItem[Math.floor(Math.random()*timeItem.length)],'hour').format('HH:mm');
         console.log("@@@@@@@@@@@@@@@@@@@@toDate@@@@@@@@@@@@@@2")*/
        return $http.get('/getDeviceHistoryById/' + fromDate + '/' + toDate + '/' + fromTime.format('HH:mm') + '/' + toTime.format('HH:mm') + '/' + deviceId)

    }
    var addDevice = function (deviceObj) {
        return $http.post('/addDevice', deviceObj);
    }
    var deleteDevice = function (deviceApiRefId) {
        return $http.get('/deleteDevice/' + deviceApiRefId);
    }
    var editDevice = function () {

    }
    var getMapPlotsFromApi = function () {
        return getDevicesFromApiAndLocal();
    }

    function getDevicesFromApiAndLocal() {
        return assetApiUrlService.getGpswoxDeviceDetails().then(function (result) {
            console.log("************result*********************")
            console.log(result.data)
            return (result.data.length > 0 ? result.data : []);
            /*  return   sensorService.getAllAssetsDetailsByType("car").then(function (response) {
             return   sensorService.getAllAssets().then(function (allAssets) {
             console.log("************result*****************")
             console.log(result)
             console.log(allAssets.data)
             console.log(response.data)
             console.log("************result*****************")

             var deviceDetailsArray=[];
             for(var j=0;j<response.data.length;j++) {
             console.log(response.data[j].sensorData.assetsLinked)
             for(var t=0;t<response.data[j].sensorData.assetsLinked.length;t++){
             if(response.data[j].sensorData.assetsLinked[t].type==="obdDevice"){
             console.log("************result*****22222222222222************")
             console.log( getDeviceDetailsByMongdbID(response.data[j].sensorData.assetsLinked[t].ref))

             }
             }

             }

             for(var  i=0;i<response.data.length;i++){
             for(var  j=0;j<result.data.length;j++){
             if(response.data[i].sensorData.uniqueIdentifier==result.data[j].device_data.imei){
             var detailsObj={};
             detailsObj.id=result.data[j].id
             detailsObj.name=response.data[i].sensorData.assetName
             detailsObj.online=result.data[j].online
             detailsObj.lat=result.data[j].lat
             detailsObj.lng=result.data[j].lng
             detailsObj.tail=result.data[j].tail
             detailsObj.time=result.data[j].time
             detailsObj.imei=result.data[j].device_data.imei
             detailsObj.created_at=new Date(result.data[j].device_data.created_at)
             detailsObj.updated_at=new Date(result.data[j].device_data.updated_at)
             deviceDetailsArray.push(detailsObj);
             }
             }
             if(response.data.length-1===i) {
             return (deviceDetailsArray.length>0?deviceDetailsArray:[]);
             }

             }

             }, function error(errResponse) {
             return errResponse;
             });

             }, function error(errResponse) {
             return errResponse;
             });*/
        }, function error(errResponse) {
            return errResponse;
        });

    }

    function setLatestDeviceId(currentDeviceId) {
        deviceId = currentDeviceId;
    }

    function getLatestDeviceId() {
        return deviceId

    }

    var latLngObj = {}

    function setLastDeviceLocation(latLng) {
        latLngObj = latLng;
    }

    function getLastDeviceLocation() {
        return latLngObj

    }

    function getDeviceDetailsByMongdbID(mongoId) {
        return $http.get('/assetByMongoDbId/' + mongoId);
    }


    var getDeviceTrackDetails = function (carId) {/*fromTime,toTime,*/
        var startDate = new Date();
        var endDate = new Date();
        endDate.setSeconds(endDate.getSeconds() - 30);
        //higher date should be down
        /*  console.log("********************service date***************************")

         console.log(new Date(startDate.getTime()))
         console.log(new Date(endDate.getTime()))
         console.log("********************service date***************************")*/
        return $http.get('/gpsData/' + endDate.getTime() + '/' + startDate.getTime() + '/' + carId);
    }

    return {
        getDevicesDetails: getDevicesDetails,
        getDeviceHistory: getDeviceHistory,
        addDevice: addDevice,
        deleteDevice: deleteDevice,
        editDevice: editDevice,
        getMapPlotsFromApi: getMapPlotsFromApi,
        setLatestDeviceId: setLatestDeviceId,
        getLatestDeviceId: getLatestDeviceId,
        setLastDeviceLocation: setLastDeviceLocation,
        getLastDeviceLocation: getLastDeviceLocation,
        getDeviceTrackDetails: getDeviceTrackDetails


    }
})



