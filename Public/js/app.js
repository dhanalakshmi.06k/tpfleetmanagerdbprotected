/**
 * Created by MohammedSaleem on 11/11/15.
 */

var dependencies = ['ui.router', 'slickCarousel', 'flashSliderUI', 'flashSlider2UI', 'flashGraphUI', 'flashCircularGraphUI', 'openlayers-directive', 'simpleJQSlider', 'hrLoaderUI'];
var taxiFleetManager = angular.module("taxiFleetManager", dependencies);

taxiFleetManager.run(function (constantService, maintainceBayService) {


    function getHistoryConstantDetails() {
        constantService.getHistroyConstants().then(function (resultDetails) {
            constantService.setDeviceHistroyConstant(resultDetails.data)

        }, function error(errResponse) {
            console.log("cannot get assets types configuration")
        })
    }

    function getMapCoordinates() {
        maintainceBayService.getMapCenterPlots().then(function (result) {
            maintainceBayService.setMapCenterDeatilsObject(result.data[0].mapDetailsSchemaModelConfiguration)

        }, function error(errResponse) {
            console.log("cannot getMapCenterPlots configuration")
        })
    }

    //get the assets types from constants configuration
    function initConfig() {
        getHistoryConstantDetails();
        getMapCoordinates();


    }

    initConfig();

})

taxiFleetManager.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('admin', {
        url: "/admin",
        templateUrl: 'Unsecure/admin.html',
        controller: 'loginCtrl'
    })
        .state('signUp', {
            url: "/signUp",
            templateUrl: 'Unsecure/signUp.html',
            controller: 'loginCtrl'
        })
        .state('app', {
            url: "/app",
            templateUrl: 'templates/app.html',
            controller: 'appController'
        })

        .state('app.live', {
            url: "/live",
            templateUrl: 'templates/live.html',
            controller: 'liveController'
        })
        .state('app.live.maintenanceBay', {
            url: "/maintenanceBay",
            templateUrl: 'templates/live/maintenanceBay.html',
            controller: 'maintenanceBayController'
        })
        .state('app.live.taxParking', {
            url: "/taxParking",
            templateUrl: 'templates/live/taxParking.html',
            controller: 'taxiParkingController'
        })
        .state('app.live.taxArrival', {
            url: "/taxArrival",
            templateUrl: 'templates/live/taxArrival.html',
            controller: 'taxiEntryExitCtrl'
        })
        .state('app.live.taxiDeparture', {
            url: "/taxiDeparture",
            templateUrl: 'templates/live/taxiDeparture.html',
            controller: 'taxiEntryExitCtrl'
        })

        .state('app.dashboard', {
            url: "/dashboard",
            templateUrl: 'templates/dashboard.html',
            controller: 'taxiDashBoardController'
        })

        .state('app.stats', {
            url: "/stats",
            templateUrl: 'templates/liveStats.html'
        })
        .state('app.stats.historicalStat', {
            url: "/historicalStat",
            templateUrl: 'templates/historicalStat.html',
            controller: 'liveController'
        })
        .state('app.stats.taxiTrail', {
            url: "/taxiTrail",
            templateUrl: 'templates/taxiTrail.html',
            controller: 'taxiTrailController'
        })

        .state('app.settings', {
            url: "/settings",
            templateUrl: 'templates/settings.html',
            controller: 'settingsCtrl'
        })
        .state('app.settings.settingsTabs', {
            url: "/settingsTabs",
            templateUrl: 'templates/settings/settingsTabs.html'
        })
        .state('app.settings.settingsTabs.simulator', {
            url: "/simulator",
            templateUrl: 'templates/settings/simulator.html',
            controller: 'simulatorCtrl'
        })
        .state('app.settings.settingsTabs.config', {
            url: "/config",
            templateUrl: 'templates/settings/config.html'
        })
        .state('app.settings.settingsTabs.users', {
            url: "/users",
            templateUrl: 'templates/settings/users.html'
        });

    $urlRouterProvider.otherwise("/admin");


    // .state('app.live.loadingBerth', {
    //     url: "/loadingBerth",
    //     templateUrl: 'templates/live/loadingBerth.html',
    //     controller: 'loadingOccupancyCtrl'
    // })
    // .state('app.live.sensors', {
    //     url: "/sensors",
    //     templateUrl: 'templates/live/sensors.html'
    // })
    //
    //
    //
    //
    // .state('app.analytics', {
    //     url: "/live",
    //     templateUrl: 'templates/live.html',
    //     controller: 'liveController'
    // })
    // .state('app.stats.predictiveStat', {
    //     url: "/predictiveStat",
    //     templateUrl: 'templates/predictiveStat.html',
    //     controller: 'liveController'
    // })


    // .state('app.settings.settingsTabs.data', {
    //     url: "/data",
    //     templateUrl: 'templates/settings/smrtData.html',
    //     controller: 'dataDownload'
    // })

});

taxiFleetManager.run(function ($rootScope, authInterceptor, settingsService) {
    $rootScope.admin = authInterceptor.getIsLoggedInStatus();
    function getAdminFlag() {
        $rootScope.admin = authInterceptor.getIsLoggedInStatus();
    }

    function getAdminStatus() {
        if ($rootScope.admin) {
            settingsService.getAdminStatusByToken().then(function (result) {
                var isUserAdmin = result.data;
                authInterceptor.setIsAdminStatus(isUserAdmin);
            })
        }
    }

    function init() {
        getAdminFlag();
        getAdminStatus();
    }

    init();
});


taxiFleetManager.directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});

taxiFleetManager.filter('setDecimal', function ($filter) {
    return function (input, places) {
        if (isNaN(input)) return input;
        // If we want 1 decimal place, we want to mult/div by 10
        // If we want 2 decimal places, we want to mult/div by 100, etc
        // So use the following to create that factor
        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
        return Math.round(input * factor) / factor;
    };
});
taxiFleetManager.directive('datePicker', function () {
    return {
        restrict: 'A',
        scope: {
            date: "="
        },
        link: function (scope, element, attrs) {
            $(element).datepicker({
                /*
                 dateFormat: "yy-mm-dd",*/
                dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
                onSelect: function (date) {
                    scope.date = date;
                    scope.date = new Date(scope.date)
                    scope.$apply();
                }
            })
        }
    };
});