/**
 * Created by zendynamix on 20/8/16.
 */




taxiFleetManager.directive("showMap", function(leafLetMapService,$timeout) {
    return {
        restrict: 'E',
        // templateUrl: '../../directives/templates/maps/googleMaps.html',
        template: "<div class='map' id='{{mapId}}'></div>",
        // controller:"mapsController",
        link: function(scope, element, attrs){
            $timeout(function () {
                leafLetMapService.initMap(25.248354, 55.352544,scope.mapId);
            },200)
        },
        scope:{
            mapId: '@'
        }
    

        
    }
});

