/**
 * Created by Suhas on 2/23/2016.
 */
var mongoose = require('mongoose'),
    busEntryExitModel = mongoose.model('busEntryExitInfo'),
    dataPusher = require('../../dataPusher');
var saveData = function (data) {
    var busEntryExitModelObj = new busEntryExitModel();

    busEntryExitModelObj = data;
    dataPusher.busEntryExitDataPusher.pushData(busEntryExitModelObj);
    /*  busEntryExitModelObj.save(function(err,result){
     if(result){
     console.log(" Bus Entry Exit detection Data Saved ")
     }
     })*/
}
module.exports = {
    saveData: saveData
}