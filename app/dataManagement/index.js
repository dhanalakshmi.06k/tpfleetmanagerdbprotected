/**
 * Created by Suhas on 9/9/2016.
 */
var rfIdDetection = require('./rfIdDetection');
var gpsData = require('./gpsData.js');
var assets = require('./assets.js');
module.exports = {
    rfIdDetection: rfIdDetection,
    gpsData: gpsData,
    assets: assets

}