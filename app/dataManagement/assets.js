/**
 * Created by Suahs on 9/11/2016.
 */
var assetModel = require('../models/sensorModel')
var getAssetsLinkedToAssetTypeProvided = function (rootAssetType, assetTypeLinked) {
    return new Promise(function (resolve, reject) {
        assetModel.aggregate([
                {$match: {"sensorData.sensorType": rootAssetType}},
                {$unwind: "$sensorData.assetsLinked"},
                {$match: {"sensorData.assetsLinked.type": assetTypeLinked}},
                {$project: {'carId': "$_id", 'carName': "$sensorData.assetName", '_id': 0}}

            ]
            , function (err, data) {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            });
    })

}

module.exports = {
    getAssetsLinkedToAssetTypeProvided: getAssetsLinkedToAssetTypeProvided
}