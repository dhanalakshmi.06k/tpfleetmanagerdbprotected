/**
 * Created by Suhas on 9/10/2016.
 */
var assetsModel = require('../models/sensorModel');
var obdDeviceTrackDataModel = require('../models/obdDeviceGpsModel');
var routeConfig = require('../../config/assetServiceConfig');
var q = require('q');
var socketConn = require('../../config/socketIo');
var request = require('request');
var gpsDataManagement = require('../dataManagement').gpsData;
var dataTrackerInfoDetails = {
    timerId: '',
    timerInterval: 10
}
var mongoose = require('mongoose');
var passtaDecModel = mongoose.model('passtadec');
var glofDataNovModel = mongoose.model('glofDataNov')
var obdDeviceGPSWOXModel = require('../models/gpswoxobddeviceModel');
var dataTrackerInfoDetails = {
    timerId: '',
    timerInterval: 10,
    timerStatus: true
}
var timerId = null;
var trailModel = mongoose.model('tripDatamodelNew');
var startTrackingOld = function () {
    stopTracking();

    if (Math.floor(Math.random() * 100) + 1 >= 45) {
        passtaDecModel.find({}, function (err, result) {
            var i = 1;
            if (err) {
                reject(new Error(err))
            }
            else {
                let thisDelay = 0;
                let interval;

                function startTimer() {

                    timerId = setTimeout(function () {
                        timeDiff = Math.abs(new Date(result[i--].dt).getTime() - new Date(result[i++].dt).getTime());
                        if (timeDiff < 15000) {
                            interval = 5;
                        }
                        else {
                            interval = 20;
                        }
                        thisDelay = timeDiff;
                        thisDelay = thisDelay

                        pushToSocket(result[i++], thisDelay);
                        startTimer();
                    }, thisDelay / interval);
                }


                startTimer();

            }
        }).sort({dt: -1});
    }
    else {
        glofDataNovModel.find({}, function (err, result) {
            var i = 1;
            if (err) {
                reject(new Error(err))
            }
            else {
                let thisDelay = 0;
                let interval;

                function startTimer() {

                    timerId = setTimeout(function () {
                        timeDiff = Math.abs(new Date(result[i--].dt).getTime() - new Date(result[i++].dt).getTime());
                        if (timeDiff < 15000) {
                            interval = 5;
                        }
                        else {
                            interval = 20;
                        }
                        thisDelay = timeDiff;
                        thisDelay = thisDelay

                        pushToSocket(result[i++], thisDelay);
                        startTimer();
                    }, thisDelay / interval);
                }


                startTimer();

            }
        }).sort({dt: -1});
    }


}
var _setInterval = require('setinterval-plus');
var currentTrackedObdId = null;
var startTrackingByOBDDevice = function (obdDeviceID) {

    var ff = new Date().getTime()
    tripTimerFunction(obdDeviceID);
    currentTrackedObdId = obdDeviceID;
    timerId = new _setInterval(function(){
        tripTimerFunction(currentTrackedObdId)
    }, 3000);

}

function tripTimerFunction(obdDeviceID) {
    trailModel.find(
        {
            $and: [
                {
                    "device_data.created_at": {$gte: new Date().getTime() - 3000}
                },
                {
                    "device_data.created_at": {$lte: new Date().getTime()}
                },
                {
                    "device_data.imei": obdDeviceID
                }

            ]
        }, function (err, result) {
            if (err) {
                reject(new Error(err))
            }
            console.log("******************result*******************" )
            console.log(result)
            socketConn.getSocketIoServer().emit('carTrackerNotifier', result);
        })
}


var i = 0;
var resArray;
var startTracking = function () {
    stopTracking();
    passtaDecModel.find({}, function (err, result) {
        resArray = result
        if (err) {
            reject(new Error(err))
        }
        else {

            if (result && result.length > 0) {

                function myTimeoutFunction() {
                    i++;
                    setTimeout(myTimeoutFunction, 1000);
                    var obj = {}
                    obj.data = resArray[i];
                    socketConn.getSocketIoServer().emit('carTrackerNotifier', obj);
                }

                myTimeoutFunction()

            }

        }


    }).sort({dt: -1});


}


function pushTestToSocket(data) {
    var obj = {}
    obj.data = data;
    socketConn.getSocketIoServer().emit('carTrackerNotifier', obj);

}


var stopTracking = function () {
    if (timerId != null) {
        timerId.stop();
    }
}

/*function startVechicletracking(){
 passtaDecModel.find({},function(err,result){
 var i=1;
 if (err){reject(new Error(err))}
 else{
 let thisDelay = 0;
 let interval;
 function startTimer() {
 console.log("start???")
 deviceTrackerInfoDetails.timerId=  setTimeout(function() {
 timeDiff = Math.abs(new Date(result[i--].dt).getTime() - new Date(result[i++].dt).getTime());
 if(timeDiff<15000){
 interval=5;
 }
 else{
 interval=20;
 }
 thisDelay = timeDiff;
 thisDelay = thisDelay
 console.log(thisDelay)
 pushToSocket(result[i++],thisDelay);
 startTimer();
 }, thisDelay/interval);
 }
 startTimer();
 }
 }).sort({dt:-1});
 }*/


function pushToSocket(data, delay) {
    var obj = {}
    obj.data = data;
    obj.delay = delay
    socketConn.getSocketIoServer().emit('carTrackerNotifier', obj);

}

var getAllDeviceData = function () {
    obdDeviceGPSWOXModel.find({}, function (err, result) {
        if (err) {
            reject(new Error(err))
        }
        else {
            /*var body = JSON.parse(allDeviceData)*/
            var promises = [];
            if (result && result.length > 0) {
                /* var obdDeviceTracked = body[0].items;*/
                for (var i = 0; i < result.length; i++) {
                    var obdDeviceObj = result[i];

                    var promise = checkObdDataExist(obdDeviceObj)
                        .then(function (result) {

                            return q(result);
                        })
                    promises.push(promise)
                }
            }
            q.all(promises).then(function (resultData) {
                var traceblecardetails = [];
                for (var i = 0; i < resultData.length; i++) {
                    var data = resultData[i];
                    if (data) {
                        traceblecardetails.push(data)
                    }
                    if (i == resultData.length - 1) {
                        pushToSocket(traceblecardetails);
                    }
                }
            })
        }
    })

    /*  var url=routeConfig.assetsApiBaseUrl+'get_devices?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;
     var options = {
     method:'get',
     url:url
     };
     request(options, function (error, response,allDeviceData) {
     if (error){reject(new Error(error))}
     else{
     var body = JSON.parse(allDeviceData)
     var promises=[];
     if(body[0] && body[0].items && body[0].items.length>0){
     var obdDeviceTracked = body[0].items;
     for(var i=0;i<obdDeviceTracked.length;i++){
     var obdDeviceObj = obdDeviceTracked[i];
     var promise=checkObdDataExist(obdDeviceObj)
     .then(function(result){
     return q(result);
     })
     promises.push(promise)
     }
     }
     q.all(promises).then(function(resultData){
     var traceblecardetails = [];
     for(var i=0;i<resultData.length;i++){
     var data = resultData[i];
     if(data){
     traceblecardetails.push(data)
     }
     if(i==resultData.length-1){
     pushToSocket(traceblecardetails);
     }
     }
     })
     }
     });*/
}

function checkObdDataExist(obdData) {

    var deferred = q.defer();
    var uniqueIdentifier = obdData.device_data.imei;

    /*    var obdDeviceTrackDataModel = new obdDeviceTrackDataModel();
     obdDeviceTrackDataModel.gpsData = obdData;
     obdDeviceTrackDataModel.save(function(err,data){
     if(err){console.log(err.stack)}else{}
     });*/
    assetsModel
        .findOne({'sensorData.uniqueIdentifier': uniqueIdentifier})
        .populate('sensorData.assetsLinked.ref')
        .exec(function (err, obDeviceData) {
            if (err) {
                deferred.reject(err)
            }
            else {
                var carTraceableDetails = {
                    carDetails: {}, driverDetails: {},
                    gpsData: {
                        speed: obdData.speed,
                        lat: obdData.lat,
                        lng: obdData.lng,
                        timeStamp: new Date(obdData.time),
                        dobDeviceRefId: obdData.id,
                        status: obdData.online
                    },
                    odbDeviceId: uniqueIdentifier
                }
                if (obDeviceData && obDeviceData.sensorData &&
                    obDeviceData.sensorData.assetsLinked &&
                    obDeviceData.sensorData.assetsLinked.length > 0) {
                    gpsDataManagement.save(obdData)
                        .then(function (gpsSavedData) {
                            checkAndGetAssetLinked(obDeviceData.sensorData.assetsLinked, 'car')
                                .then(function (carLinked) {
                                    if (carLinked) {
                                        gpsSavedData.carLinked = {
                                            id: carLinked.ref._id,
                                            assetData: carLinked.ref.sensorData
                                        };
                                        var carId = carLinked.ref._id;
                                        carTraceableDetails.carDetails = carLinked.ref;
                                        getAssetsDetailsWithRefAssetsLinked(carId)
                                            .then(function (carLinkedDetails) {
                                                if (carLinkedDetails && carLinkedDetails.sensorData &&
                                                    carLinkedDetails.sensorData.assetsLinked &&
                                                    carLinkedDetails.sensorData.assetsLinked.length > 0) {
                                                    checkAndGetAssetLinked(carLinkedDetails.sensorData.assetsLinked, 'driver')
                                                        .then(function (driverDetails) {
                                                            if (driverDetails) {
                                                                gpsSavedData.driverLinked = {
                                                                    id: driverDetails.ref._id,
                                                                    assetData: driverDetails.ref.sensorData
                                                                };
                                                                gpsDataManagement.update(gpsSavedData);
                                                                carTraceableDetails.driverDetails = driverDetails.ref;
                                                                deferred.resolve(carTraceableDetails);
                                                            } else {
                                                                gpsDataManagement.update(gpsSavedData);
                                                                deferred.resolve(carTraceableDetails);
                                                                /*console.warn('Driver Not Linked To Car Which Is linked To Obd' +
                                                                 ' Device With UniqueIdentifier '+uniqueIdentifier)*/
                                                            }

                                                        })
                                                } else {
                                                    gpsDataManagement.update(gpsSavedData);
                                                    deferred.resolve(carTraceableDetails);
                                                    /*console.warn('Driver Not Linked To Car Which Is linked To Obd' +
                                                     ' Device With UniqueIdentifier '+uniqueIdentifier)*/
                                                }

                                            })
                                    } else {
                                        deferred.resolve(null);
                                        /*console.warn('Car Not Linked To Obd Device With UniqueIdentifier '+uniqueIdentifier)*/
                                    }
                                })
                        })
                } else {
                    /*console.warn('Car Not Linked To Obd Device With UniqueIdentifier '+uniqueIdentifier)*/
                    deferred.resolve(null);
                }
            }
        })
    return deferred.promise;
}

function checkAndGetAssetLinked(assetsLinkedArray, type) {
    return new Promise(function (resolve, reject) {
        var relatedAssets = null;
        for (var i = 0; i < assetsLinkedArray.length; i++) {
            var assetsLinkedObj = assetsLinkedArray[i];
            if (assetsLinkedObj.type == type) {
                relatedAssets = assetsLinkedObj;
            }
            if (i == assetsLinkedArray.length - 1) {
                resolve(relatedAssets);
            }
        }
    })
}

function getAssetsDetailsWithRefAssetsLinked(assetId) {
    return new Promise(function (resolve, reject) {
        assetsModel.findOne({_id: assetId})
            .populate('sensorData.assetsLinked.ref')
            .exec(function (err, result) {
                if (err) {
                    reject(err)
                } else {
                    resolve(result)
                }
            })
    })
}

/*function pushToSocket(data){
 socketConn.getSocketIoServer().emit('carTrackerNotifier',data);
 console.log('No of Cars linked '+data)
 }*/


module.exports = {
    dataTrackerInfoDetails: dataTrackerInfoDetails,
    startTracking: startTracking,
    stopTracking: stopTracking,
    getAllDeviceData: getAllDeviceData,
    startTrackingByOBDDevice: startTrackingByOBDDevice
}