/**
 * Created by Flashbox on 8/14/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    SensorsModel = mongoose.model('sensors');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};

router.use('/api', expressJwt({secret: secret}));
router.use(function (err, req, res, next) {
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});

router.get('/api/sensorsData/:type', function (req, res) {
    SensorsModel.find({"sensorData.sensorType": req.params.type}, function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {
            res.send(result)
        }
    });
})

router.post('/notifyAdmin', function (req, res, next) {
    console.log("&&&&&&&&&&&&&&&&&&&&&&&")
    console.log(req.body)
    sendEmailWithPassword(req.body)

})
var nodemailer = require('nodemailer');
function sendEmailWithPassword(user,res) {

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
//       secure: true,
        secureConnection: false,// true for 465, false for other ports
        auth: {
            user: 'passwrdalert@gmail.com',
            pass: 'pass@word123'
        },
        tls:{
            rejectUnauthorized:false
        }
    });

    var mailOptions = {
        from: 'passwrdalert@gmail.com',
        to: 'passwrdalert@gmail.com',
        subject: 'Password Notification Alert',
        text: "Registred Details",
        html:'<h1>'
        +'Device Name'+"----"+user.deviceName+
        'Speed of the vehicle'+"----"+user.currentSpeed+
        'Notification Type'+"----"+"speedNotification"+
        '</h1>'
    };


    /*,
     html: '<h4>messageStr</h4>'*/
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            res.send(info.response);
        }
    });
}