/**
 * Created by Suhas on 9/11/2016.
 */
var cardDetails = require('../../config/cardDetails').carDetails;
var socketConn = require('../../config/socketIo');
var dynamicCardDataDetails = {
    timerId: '',
    timerInterval: 5,
    previousData: new Array(cardDetails.length).fill(null)
}
var startGeneratingDynamicData = function () {
    dynamicCardDataDetails.timerId = setInterval(function () {
        pushCardDetails();
        dynamicCardDataDetails.timerInterval = Math.round(Math.floor(Math.random() * 3) + 1);
    }, dynamicCardDataDetails.timerInterval * 1000)

}
var pushCardDetails = function () {
    var cardDetailsArray = cardDetails;
    var dynamicValues = [];
    cardDetailsArray.forEach(function (cardObj, index) {
        generateDynamicValue(cardObj, index)
            .then(function (data) {
                dynamicValues.push(data);
                dynamicCardDataDetails.previousData[index] = data.value;
                if (dynamicValues.length == cardDetailsArray.length) {
                    pushToSocket(dynamicValues, index);
                }
            })
    })
}
function generateDynamicValue(cardObjDetails, index) {
    return new Promise(function (resolve, reject) {
        var booleandata = [true, false]
        var min = parseInt(cardObjDetails.min);
        var max = parseInt(cardObjDetails.max);
        var previousData = 0;
        if (dynamicCardDataDetails.previousData[index]) {
            previousData = dynamicCardDataDetails.previousData[index];
        }
        var randomBooleanVal = booleandata[Math.round(Math.floor(Math.random() * booleandata.length) + 0)];
        var randomValue = 0;
        if (randomBooleanVal) {
            if (dynamicCardDataDetails.previousData[index]) {
                randomValue = previousData;
            }
            else {
                randomValue = Math.round(Math.floor(Math.random() * (max - min + 1) + min));
            }
        } else {
            randomValue = Math.round(Math.floor(Math.random() * (max - min + 1) + min));
        }
        if (randomValue > max) {
            randomValue = (randomValue % max) + min
        }
        var data = {
            cardName: cardObjDetails.cardName,
            value: randomValue
        }
        resolve(data)
    })

}


function pushToSocket(data) {
    socketConn.getSocketIoServer().emit('cardDetails', data);
}


var stopGeneratingDynamicData = function () {
    clearInterval(dataTrackerInfoDetails.timerId)
}

module.exports = {
    dynamicCardDataDetails: dynamicCardDataDetails,
    stopGeneratingDynamicData: stopGeneratingDynamicData,
    startGeneratingDynamicData: startGeneratingDynamicData,
    pushCardDetails: pushCardDetails
}