/**
 * Created by Suhas on 6/24/2016.
 */
const smrt = {
    'BusEntryExitDetails_directions_entry': 'entrance',
    'BusEntryExitDetails_directions_entry': 'exit'
}
function getConstantValues() {
    return smrt
}
module.export = {
    getConstantValues: getConstantValues
}