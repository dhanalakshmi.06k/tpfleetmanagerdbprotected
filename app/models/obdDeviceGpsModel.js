/**
 * Created by Suhas on 9/10/2016.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var obdDeviceGpsSchema = new mongoose.Schema(
    {
        gpsData: {},
        carLinked: {},
        driverLinked: {},
        created_at: {type: Date},
        updated_at: {type: Date}
    }, {collection: "obdDeviceGpsDoc"});

module.exports = mongoose.model('obdDeviceGpsModel', obdDeviceGpsSchema);

obdDeviceGpsSchema.pre('save', function (next) {
    var now = Date.now();
    console.log()
    this.updated_at = now;
    if (!this.created_at) {
        this.created_at = now;
    }
    next();
});
obdDeviceGpsSchema.post('save', function (doc) {
    console.log('%s gps Data of has been saved');
});