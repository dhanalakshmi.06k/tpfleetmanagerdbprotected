/**
 * Created by dhanalakshmi on 30/11/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var golfNovSchema = new mongoose.Schema(
    {
        dt: String,
        lat: Number,
        lng: Number,
        altitude: Number,
        angle: Number,
        speed: Number
    }, {collection: "passtadec"});

module.exports = mongoose.model('passtadec', golfNovSchema);