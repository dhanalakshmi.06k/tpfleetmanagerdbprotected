/**
 * Created by dhanalakshmi on 28/11/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var golfNovSchema = new mongoose.Schema(
    {
        dt: String,
        lat: Number,
        lng: Number,
        altitude: Number,
        angle: Number,
        speed: Number
    }, {collection: "glofDataNov"});

module.exports = mongoose.model('glofDataNov', golfNovSchema);
