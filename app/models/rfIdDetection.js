/**
 * Created by Suhas on 9/6/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var sensorModel = require('./sensorModel.js');
var rfIdDetectedSchema = new Schema({
    detectionTime: Date,
    rfIdNumber: String,
    direction: String,
    strength: Number,
    antennaNo: Number,
    count: Number,
    carLinked: {registrationNumber: String, carNo: String, dutyType: String},
    driverLinked: {driverName: String, badgeNo: String, driverId: String},
    created_at: {type: Date},
    updated_at: {type: Date}
}, {collection: "rfIdDetectionDoc"})
module.exports = mongoose.model('rfIdDetectionModel', rfIdDetectedSchema);

rfIdDetectedSchema.pre('save', function (next) {
    console.log("RFID Detection Data Saved")
    var now = new Date().toU;
    this.updated_at = now;
    if (!this.created_at) {
        this.created_at = now;
    }
    next();
});