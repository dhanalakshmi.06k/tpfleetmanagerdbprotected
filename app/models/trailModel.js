/**
 * Created by dhanalakshmi on 14/12/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var tripDetailsSchema = new Schema(
    {
        "id": Number,
        "name": String,
        "online": String,
        "alarm": {type: Number, default: 0},
        "time": String,
        "timestamp": {type: Number, default: 0},
        "acktimestamp": {type: Number, default: 0},
        "speed": {type: Number, default: 0},
        "lat": {type: Number, default: 0},
        "lng": {type: Number, default: 0},
        "course": {type: String, default: "-"},
        "power": {type: String, default: "-"},
        "altitude": {type: String, default: "-"},
        "address": {type: String, default: "-"},
        "protocol": {type: String, default: "-"},
        "driver": {type: String, default: "-"},
        "driver_data": {
            "id": {type: Number, default: null},
            "user_id": {type: Number, default: null},
            "device_id": {type: Number, default: null},
            "name": {type: Number, default: null},
            "rfid": {type: Number, default: null},
            "phone": {type: Number, default: null},
            "email": {type: Number, default: null},
            "description": {type: Number, default: null},
            "created_at": {type: Number, default: null},
            "updated_at": {type: Number, default: null},
        },
        "sensors": [],
        "services": [],
        "tail": [],
        "distance_unit_hour": {type: String, default: "kph"},
        "device_data": {
            "id": Number,
            "user_id": Number,
            "traccar_device_id": Number,
            "icon_id": Number,
            "active": Number,
            "deleted": Number,
            "name": String,
            "imei": String,
            "fuel_measurement_id": Number,
            "fuel_quantity": String,
            "fuel_price": String,
            "fuel_per_km": String,
            "sim_number": {type: String, default: null},
            "device_model": {type: String, default: null},
            "plate_number": {type: Number, default: null},
            "vin": {type: Number, default: null},
            "registration_number": {type: Number, default: null},
            "object_owner": {type: String, default: "admin@yourdomain.com"},
            "expiration_date": {type: String, default: "0000-00-00"},
            "tail_color": {type: String, default: "#33CC33"},
            "tail_length": {type: Number, default: 5},
            "engine_hours": {type: String, default: "gps"},
            "detect_engine": {type: String, default: "gps"},
            "min_moving_speed": {type: Number, default: 6},
            "min_fuel_fillings": {type: Number, default: 10},
            "min_fuel_thefts": {type: Number, default: 10},
            "snap_to_road": {type: Number, default: 0},
            "gprs_templates_only": {type: Number, default: 0},
            "created_at": Date,
            "updated_at": Date,
            "pivot": {
                "user_id": {type: Number, default: 2},
                "device_id": {type: Number, default: 258},
                "group_id": {type: Number, default: 0},
                "current_driver_id": {type: Number, default: null},
                "active": {type: Number, default: 1},
                "timezone_id": {type: Number, default: null},
            },
            "group_id": {type: Number, default: 0},
            "current_driver_id": {type: Number, default: 0}
        }

    }

    , {collection: "tripDatamodelNew"})

module.exports = mongoose.model('tripDatamodelNew', tripDetailsSchema);

