/**
 * Created by Suhas on 2/23/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var busEntryExitSchema = new Schema({
    "taxiRegistrationNumber": String,
    "taxiNumber": String,
    "arrivalTimeStamp": Date,
    "departureTimeStamp": Date,
    "diverName": String
}, {collection: "busEntryExitDoc"});
/*busEntryExitSchema.index({ 'busEntryExitInformation.receivedDateTime': 1});*/
busEntryExitSchema.index({'receivedDateTime': 1});
busEntryExitSchema.plugin(random);
module.exports = mongoose.model('busEntryExitInfo', busEntryExitSchema);