/**
 * Created by dhanalakshmi on 4/9/16.
 */



var mongoose = require('mongoose'),
    config = require('../../config/config'),
path = require("path");
mapDetailsModel = require('../models/liveAndDetailsMapCoordinatesmodel');
Schema = mongoose.Schema;
fs = require('fs');

mapDetailsModel.remove({}, function (err) {
    console.log("previous data removed")
    //generateMapCoordinates();
})

var configJson={
    "maptype": {
        "bingMap": {
            "key": "AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L"
        }
    },
    "detailsTab": {
        "terminal1": {
            "mapCenterCoOrdinates": {
                "lat": 25.248354,
                "lng": 55.352544,
                "zoomLevel": 20
            }
        },
        "terminal2": {
            "mapCenterCoOrdinates": {
                "lat": 25.260665,
                "lng": 55.372815,
                "zoomLevel": 17
            }
        },
        "terminal3": {
            "mapCenterCoOrdinates": {
                "lat": 25.243131,
                "lng": 55.372620,
                "zoomLevel": 17
            }
        }
    },
    "liveTab": {
        "onRoad": {
            "mapCenterCoOrdinates": {
                "lat": 25.071614,
                "lng": 55.280147,
                "zoomLevel": 10.5
            }
        },
        "DXB": {
            "mapCenterCoOrdinates": {
                "lat": 25.2516641,
                "lng": 55.3688243,
                "zoomLevel": 14.5
            }
        },
        "DWC": {
            "mapCenterCoOrdinates": {
                "lat": 25.8945412,
                "lng": 55.1563713,
                "zoomLevel": 15
            }
        },
        "jebelAli": {
            "mapCenterCoOrdinates": {
                "lat": 25.974922,
                "lng": 55.013379,
                "zoomLevel": 12.5
            }
        },
        "Expo2020": {
            "mapCenterCoOrdinates": {
                "lat": 25.974922,
                "lng": 55.013379,
                "zoomLevel": 12.5
            }
        },
        "Depo": {
            "mapCenterCoOrdinates": {
                "lat": 25.2779218,
                "lng": 55.4179614,
                "zoomLevel": 17
            }
        }
    }
}



function generateMapDetails(callBack) {
    var configObj = new mapDetailsModel();

    var pathVal = path.join(__dirname, '..','..','config/mapCenterCoordinates.json');
    fs.readFile(pathVal, function (err, data) {
        if (err) throw err;
        configObj.mapDetailsSchemaModelConfiguration = JSON.parse(data);

        configObj.save(function(res,err) {
            callBack();
        });

    });




};
module.exports = {
    run: generateMapDetails
}
