/**
 * Created by zendynamix on 9/11/2016.
 */
var carDetails = [
    {
        cardName: 'DXB',
        min: 457,
        max: 467
    },
    {
        cardName: 'DWC',
        min: 300,
        max: 313
    },
    {
        cardName: 'Jebel Ali',
        min: 523,
        max: 535
    },
    {
        cardName: 'Expo 2020',
        min: 120,
        max: 130
    }, {
        cardName: 'Depo',
        min: 1444,
        max: 1458
    }
]

module.exports = {carDetails: carDetails}